#!/usr/bin/php
<?PHP

include_once ( '../public_html/php/common.php' ) ;

$j1 = json_decode ( file_get_contents ( 'https://www.wikidata.org/w/api.php?action=query&list=prefixsearch&psnamespace=2&pslimit=500&pssearch=User:Pasleim/projectmerge/&format=json&psoffset=0' ) ) ;
$j2 = json_decode ( file_get_contents ( 'https://www.wikidata.org/w/api.php?action=query&list=prefixsearch&psnamespace=2&pslimit=500&pssearch=User:Pasleim/projectmerge/&format=json&psoffset=500' ) ) ;
$pages = array_merge ( $j1->query->prefixsearch , $j2->query->prefixsearch ) ;

$data = array() ;
foreach ( $pages AS $p ) {
	$url = "https://www.wikidata.org/w/index.php?title=" . myurlencode($p->title) . "&action=raw" ;
	$wiki = file_get_contents ( $url ) ;
	$rows = explode ( "\n" , $wiki ) ;
	foreach ( $rows AS $row ) {
		if ( !preg_match ( '/^\#\s*\[\[Q(\d+)\]\].+\[\[Q(\d+)\]\]/' , $row , $m ) ) continue ;
		$q1 = $m[1] * 1 ;
		$q2 = $m[2] * 1 ;
		if ( $q1 > $q2 ) list($q1,$q2) = array($q2,$q1) ;
		$data["$q1"]["$q2"] = 1 ;
	}
}

$out = array() ;
foreach ( $data AS $q1 => $matches ) {
	foreach ( $matches AS $q2 => $dummy ) $out[] = array ( $q1 , $q2 ) ;
}

$outfile = '/data/project/wikidata-todo/public_html/pasleim_projectmerge.json' ;
$fh = fopen ( $outfile , 'w' ) ;
fwrite ( $fh , json_encode ( $out ) ) ;
fclose ( $fh ) ;

?>