#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

function my_die ( $s ) {
	global $wiki , $ts ;
	print "ERROR: $wiki $ts: $s\n" ;
	exit ( 0 ) ;
}

$wiki = $argv[1] ;

//if ( $wiki == 'commonswiki' ) exit ( 0 ) ;
if ( $wiki == 'wikidatawiki' ) exit ( 0 ) ;
if ( $wiki == 'metawiki' ) exit ( 0 ) ;
if ( $wiki == 'nostalgiawiki' ) exit ( 0 ) ;

$server = getWebserverForWiki ( $wiki ) ;

$ns = array() ;
$url = "https://$server/w/api.php?action=query&meta=siteinfo&siprop=namespaces&format=json" ;
$f = @file_get_contents ( $url ) ;
if ( $f === false ) my_die ( "Could not get namespaces" ) ;

$j = json_decode ( $f ) ;
if ( $j === null ) my_die ( "Namespaces not JSON" ) ;

$star = '*' ;
foreach ( $j->query->namespaces AS $k => $v ) {
	$ns[$k] = $v->$star ;
}

$ts = date ( 'YmdHis' , strtotime ( '-2 week' ) ) ;

#print "$wiki\n" ;

$db2 = openDBwiki ( $wiki , true ) ;
if ( !isset($db2) or $db2 === null or !is_object($db2) ) my_die ( "Could not open wiki DB" ) ;

$db = openToolDB ( 'duplicity_p' ) ;
if ( !isset($db) or $db === null or !is_object($db) ) my_die ( "Could not open tool DB" ) ;
$sql = "UPDATE no_wd SET tmp_flag=1 WHERE wiki='$wiki'" ;
if(!$result = $db->query($sql)) my_die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");



$cats = array() ;
$sql = "SELECT * FROM bad_cats WHERE wiki='$wiki'" ;
if(!$result = $db->query($sql)) my_die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$cats[] = $db2->real_escape_string ( ucfirst ( str_replace ( ' ' , '_' , trim ( utf8_encode($o->cat) ) ) ) ) ;
}
$cats = '"' . implode ( '","' , $cats ) . '"' ;

// Patch "mysql server has gone away" labs bug
$db2 = openDBwiki ( $wiki , true ) ;

$sql = 'select page_title,page_namespace,rev_timestamp from page,revision where rev_parent_id=0 and page_id=rev_page and page_namespace=0 and page_is_redirect=0 and rev_timestamp<"'.$ts.'"' ;
$sql .= ' and not exists (select * from wikidatawiki_p.wb_items_per_site where ips_site_id="'.$wiki.'" and ips_site_page=replace(page_title,"_"," "))' ;
#$sql .= " AND page_id NOT IN (SELECT pp_page FROM page_props WHERE pp_propname='wikibase_item')" ;
$sql .= ' AND NOT EXISTS (select * from categorylinks where cl_from=page_id and cl_to in ('.$cats.'))' ;
if(!$result = $db2->query($sql)) my_die('There was an error running the query [' . $db2->error . ']'." on $wiki:\n$sql\n\n");
$sql = array() ;
while($o = $result->fetch_object()){
	$title = $o->page_title ;
	if ( $ns[$o->page_namespace] != '' ) $title = $ns[$o->page_namespace] . ':' . $title ; // Just in case...
//	$title = utf8_decode ( $title ) ;
	$title = $db2->real_escape_string ( str_replace ( '_' , ' ' , $title ) ) ;
	$sql[] = "INSERT INTO no_wd (wiki,title,creation_date,tmp_flag,random) VALUES ('$wiki','$title','{$o->rev_timestamp}',0,rand()) ON DUPLICATE KEY UPDATE tmp_flag=0" ;
//	if(!$result2 = $db->query($sql)) my_die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
}

// Remove non-existant ones
$sql[] = "DELETE FROM candidates WHERE no_wd_id IN (SELECT id FROM no_wd WHERE tmp_flag=1)" ;
$sql[] = "DELETE FROM no_wd WHERE tmp_flag=1" ;
$db = openToolDB ( 'duplicity_p' ) ;
foreach ( $sql AS $s ) {
	if(!$result2 = $db->query($s)) my_die('There was an error running the query [' . $db->error . ']'."\n$s\n\n");
}

// Update stats
$ts = date ( 'Ymd' ) ;
$sql = "INSERT IGNORE INTO stats (wiki,date,count) VALUES ('$wiki','$ts',(SELECT count(*) FROM no_wd WHERE wiki='$wiki'))" ;
if(!$result = $db->query($sql)) my_die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");

// Fix candidates
$db2 = openDB ( 'wikidata' , 'wikidata' , true ) ;
$sql = "select distinct q from candidates,no_wd where no_wd.id=no_wd_id and wiki='$wiki' and checked=0" ;
if(!$result = $db->query($sql)) my_die('There was an error running the query A [' . $db->error . ']'."\n$sql\n\n");
$qs = array() ;
while($o = $result->fetch_object()) $qs[] = $o->q ;
$qs = implode ( ',' , $qs ) ;
if ( $qs != '' ) {
	$sql = "select distinct ips_item_id from wb_items_per_site where ips_site_id='$wiki' and ips_item_id in ($qs)" ;
	$qs = array() ;
	if(!$result = $db2->query($sql)) my_die('There was an error running the query B [' . $db2->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()) $qs[] = $o->ips_item_id ;
	$qs = implode ( ',' , $qs ) ;
	if ( $qs != '' ) {
		$sql = "delete from candidates where no_wd_id IN (select id FROM no_wd WHERE wiki='$wiki') and checked=0 and q IN ($qs)" ;
		if(!$result = $db->query($sql)) my_die('There was an error running the query C [' . $db->error . ']'."\n$sql\n\n");
	}
}

print "Done\t$wiki\t$ts\n" ;

?>