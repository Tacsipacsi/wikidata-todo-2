#!/usr/bin/php
<?PHP

require_once ( '/data/project/wikidata-todo/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/wikidata-todo/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

# Config
$params = [
	'id_prop' => 'P1708' ,
	'group' => 'DENKMALLISTE_SACHSEN' ,
	'language' => 'de' ,
	'wiki' => 'dewiki' ,
	'entry_template' => 'Denkmalliste Sachsen Tabellenzeile' ,
	'item_hint_key' => 'name' ,
	'petscan' => [
		'language' => 'de' ,
		'depth' => '9' ,
		'categories' => 'Liste (Kulturdenkmale in Sachsen)' ,
		'templates_yes' => 'Denkmalliste Sachsen Tabellenzeile' ,
	] ,
	'formatID' => function ( $id ) {
		//return preg_replace ( '/^0+/' , '' , $id ) ;  // Trim leading zeros
		return str_pad ( $id , 8 , '0' , STR_PAD_LEFT ) ; // Use leading zeros
	}
] ;


#____________________________________________________________________________________________________________

$main_table = 'denkmallisten' ;


function parseMultilineTemplates ( $wikitext , $template , $params ) {
	$ret = [] ;

	# Generate main template pattern
	$template_first_letter = substr ( trim($template) , 0 , 1 ) ;
	$template_first_letter = '[' . strtoupper($template_first_letter) . strtolower($template_first_letter) . ']' ;
	$template_rest = substr ( trim($template) , 1 ) ;
	$template_pattern = '/^\{\{' . $template_first_letter . preg_replace ( '/[ _]/' , '[ _]' , $template_rest ) . '\s*$/' ;
	
	$last_key = '' ;
	$in_template = false ;
	$current_object = [] ;
	$rows = explode ( "\n" , $wikitext ) ;
	foreach ( $rows AS $row ) {
		if ( preg_match ( $template_pattern , $row ) ) {
			if ( $in_template ) $ret[] = $current_object ;
			$current_object = [] ;
			$in_template = true ;
			$last_key = '' ;
			continue ;
		}
		if ( preg_match ( '/^\s*\}\}\s*$/' , $row ) ) {
			if ( $in_template ) $ret[] = $current_object ;
			$current_object = [] ;
			$in_template = false ;
			continue ;
		}
		if ( !$in_template ) continue ; // Non-template row
		if ( preg_match ( '/^\s*\|\s*(.+?)\s*=(.*)$/' , $row , $m ) ) {
			$last_key = trim ( strtolower ( $m[1] ) ) ;
			$current_object[$last_key] = trim($m[2]) ;
			continue ;
		}
		if ( $last_key != '' ) $current_object[$last_key] .= " " . trim($row) ; # Fallback for multi-line value
	}
	if ( $in_template ) $ret[] = $current_object ;

	if ( isset($params['formatID']) ) {
		foreach ( $ret AS $k => $v ) {
			if ( isset($v['id']) ) $ret[$k]['id'] = $params['formatID'] ( $v['id'] ) ;
		}
	}

	return $ret ;
}

function getWikiLinkTargets ( $wikitext ) {
	if ( !preg_match_all ( '/\[\[\s*(.+?)\s*[\#\|\]]/' , $wikitext , $m ) ) return [] ;
	return $m[1] ;
}

function getItemForWikipediaPage ( $wiki , $page_name ) {
	global $tfc  ;
	$page_name = ucfirst ( trim ( str_replace ( '_' , ' ' , $page_name ) ) ) ;
	$dbwd = $tfc->openDBwiki ( 'wikidatawiki' , true ) ;
	$sql = "SELECT * FROM wb_items_per_site where ips_site_id='$wiki' AND ips_site_page='" . $dbwd->real_escape_string($page_name) . "'" ;
	$result = $tfc->getSQL ( $dbwd , $sql ) ;
	while($o = $result->fetch_object()) return 'Q' . $o->ips_item_id ;
}

function getHeaderFromWikitext ( $wikitext ) {
	$header = '' ;
	$rows = explode ( "\n" , $wikitext ) ;
	foreach ( $rows AS $row ) {
		if ( preg_match ( '/^\s*==/' , $row ) ) break ;
		$header .= "$row " ;
	}
	return $header ;
}

function getPlaceItem ( $page , $wikitext , $wiki ) {
	global $tfc  ;

	# Get place name from page title
	$page_spaces = str_replace ( '_' , ' ' , $page ) ;
	$page_spaces = preg_replace ( '/, [A-Z][a-z]*–[A-Z][a-z]*$/' , '' , $page_spaces ) ;
	if ( !preg_match ( '/^Liste der Kulturdenkmale (in|der Stadt) (.+)$/' , $page_spaces , $m ) ) return ;
	$place_name = trim ( str_replace ( '_' , ' ' , $m[2] ) ) ;
	if ( $place_name == '' ) return ; // Paranoia

	# Get header text
	$header = getHeaderFromWikitext ( $wikitext ) ;

	# Try to find relevant link in the header
	$place_names = [ $place_name ] ;
	if ( preg_match ( '/^Stadt (.+)$/' , $place_name , $m ) ) $place_names[] = $m[1] ;
	if ( preg_match ( '/^(.+)-(.+?)$/' , $place_name , $m ) ) $place_names[] = $m[1] ; 
	if ( preg_match ( '/^(.+?)-(.+)$/' , $place_name , $m ) ) $place_names[] = $m[1] ; 
	if ( preg_match ( '/^(.+) \(.+$/' , $place_name , $m ) ) $place_names[] = $m[1] ; 
	if ( preg_match ( '/^(.+) (Nord|Süd|Ost|West)$/' , $place_name , $m ) ) $place_names[] = $m[1] ; 
	$targets = getWikiLinkTargets ( $header ) ;
	foreach ( $place_names AS $pn ) {
		foreach ( $targets AS $target ) {
			$target = str_replace ( '_' , ' ' , $target ) ;
			if ( $pn == $target  ) return getItemForWikipediaPage($wiki,$target) ;
			if ( substr ( $target , 0 , strlen($pn)+1 ) == "$pn " ) return getItemForWikipediaPage($wiki,$target) ;
		}
	}
}

function getFieldFromEntry ( $entry , $field ) {
	if ( isset($entry[$field]) ) return trim($entry[$field]) ;
	return '' ;
}


function getQS () {
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/wikidata-todo/reinheitsgebot.conf" ;
	$qs->toolname = 'denkmallisten_sachsen_magnus' ;
	$qs->sleep = 1 ;
	$qs->use_command_compression = true ;
	return $qs ;
}

function getInstanceOf ( $entry ) {
	$text = getFieldFromEntry ( $entry , 'name' ) . '; ' . getFieldFromEntry ( $entry , 'beschreibung' ) ;
	$q_p31 = 'Q1516079' ; // Default
	if ( preg_match ( '/Wohnhaus/i' , $text ) ) $q_p31 = 'Q3947' ; // TODO find better one
	else if ( preg_match ( '/Mietshaus/i' , $text ) ) $q_p31 = 'Q1577547' ; // TODO find better one
	else if ( preg_match ( '/schule\b/i' , $text ) ) $q_p31 = 'Q3914' ;
	else if ( preg_match ( '/kapelle/i' , $text ) ) $q_p31 = 'Q108325' ;
	else if ( preg_match ( '/brücke/i' , $text ) ) $q_p31 = 'Q12280' ;
	else if ( preg_match ( '/pfarrhaus/i' , $text ) ) $q_p31 = 'Q607241' ;
	else if ( preg_match ( '/krankenhaus/i' , $text ) ) $q_p31 = 'Q16917' ;
	else if ( preg_match ( '/haus/i' , $text ) ) $q_p31 = 'Q3947' ;
	else if ( preg_match ( '/kloster/i' , $text ) ) $q_p31 = 'Q44613' ;
	else if ( preg_match ( '/kirche/i' , $text ) ) $q_p31 = 'Q16970' ;
	else if ( preg_match ( '/Burg/' , $text ) ) $q_p31 = 'Q23413' ;
	else if ( preg_match ( '/villa/i' , $text ) ) $q_p31 = 'Q3950' ;
	else if ( preg_match ( '/gasthaus/i' , $text ) ) $q_p31 = 'Q5526694' ;
	else if ( preg_match ( '/denkmal/i' , $text ) ) $q_p31 = 'Q15077340' ;
	else if ( preg_match ( '/brunnen/i' , $text ) ) $q_p31 = 'Q43483' ;
	else if ( preg_match ( '/schloss\b/i' , $text ) ) $q_p31 = 'Q16823155' ;
	return $q_p31 ;
}

function convertToUTF8($text){
	$encoding = mb_detect_encoding($text, mb_detect_order(), false);
	if ( $encoding == "UTF-8" ) {
		$text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');    
	}
	$ret = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);
	return $ret;
}

function fixWikidataStringParameter ( $s ) {
	global $tfc ;
	if ( preg_match ( '/^[PQ]\d+$/i' , $s ) ) return $s ; // Skip items/properties
	$s = convertToUTF8 ( $s ) ;
	$s = $tfc->trimWikitextMarkup ( $s ) ;
	$s = str_replace ( '"' , '' , $s ) ;
	$s = str_replace ( "\t" , ' ' , $s ) ;
	$s = trim ( $s ) ;
	$s = substr ( $s , 0 , 250 ) ;
	return $s ;
}

function doesWikidataItemWithLabelAndDescriptionExist ( $language , $label , $description ) {
	global $tfc ;
	$dbwd = $tfc->openDBwiki ( 'wikidatawiki' , true ) ;
	$sql = "SELECT term_entity_id
			FROM wb_terms t1
			WHERE term_entity_type='item'
			AND term_language='" . $dbwd->real_escape_string($language) . "'
			AND term_type='label'
			AND term_text='" . $dbwd->real_escape_string($label) . "'
			AND EXISTS (
			SELECT *
			FROM wb_terms t2
			WHERE t1.term_full_entity_id=t2.term_full_entity_id
			AND t2.term_entity_type='item'
			AND t2.term_language=t1.term_language
			AND t2.term_type='description'
			AND t2.term_text='" . $dbwd->real_escape_string($description) . "'
			) LIMIT 1" ;
	$result = $tfc->getSQL ( $dbwd , $sql ) ;
	while($o = $result->fetch_object()) return true ;
	return false ;
}

function enforceUniqueWikidataDescription ( $language , $label , $description ) {
	$ret = '' ;
	$cnt = 0 ;
	while ( 1 ) {
		if ( $cnt == 0 ) $ret = substr ( $description , 0 , 250 ) ;
		else $ret = substr ( $description , 0 , 240 ) . " (#$cnt)" ;
		if ( !doesWikidataItemWithLabelAndDescriptionExist ( $language , $label , $ret ) ) break ;
		$cnt++ ;
	}
	return $ret ;
}

function doesFileExistOnCommons ( $file ) {
	global $tfc ;
	$file = ucfirst ( trim ( $file ) ) ;
	if ( $file == '' ) return false ;
	$url = "https://commons.wikimedia.org/w/api.php?action=query&titles=File:" . $tfc->urlEncode($file) . "&prop=imageinfo&format=json" ;
	$j = json_decode ( @file_get_contents($url) ) ;
	if ( !isset($j) or $j === null ) return false ;
	if ( !isset($j->query) ) return false ;
	if ( !isset($j->query->pages) ) return false ;
	$minus1 = '-1' ;
	if ( isset($j->query->pages->$minus1) ) return false ;
	return true ;
}

function addOrAmendEntry ( $entry , $bespoke_statements , $params ) {
	global $tfc , $id2q , $qs ;
	
	$commands = [] ;

	$id = getFieldFromEntry ( $entry , 'id' ) ;
	$id = $params['formatID'] ( $id ) ;
	if ( $id == '' or preg_match ( '/^0+$/',$id) ) {
		print "BAD ID:$id\n" ;
		print_r ( $entry ) ;
		return  ;
	}

	$lang = $params['language'] ;

	$i = new WDI ;
	$q = 'LAST' ;
	if ( isset($id2q[$id]) ) {
		$q = $id2q[$id] ;
		$wil = new WikidataItemList ;
		$wil->loadItem ( $q ) ;
		$i = $wil->getItem ( $q ) ;
//	} else if ( isset($params['item_hint_key']) and preg_match ( '/\[\[/' , getFieldFromEntry ( $entry , $params['item_hint_key'] ) ) ) { // No item yet, but has a wiki link
	} else if ( isset($params['item_hint_key']) and preg_match ( '/^\s*\[\[[^\#]+?\]\]/' , getFieldFromEntry ( $entry , $params['item_hint_key'] ) ) ) { // No item yet, but starts with a non-hashed a wiki link
		$targets = getWikiLinkTargets ( getFieldFromEntry ( $entry , $params['item_hint_key'] ) ) ;
		if ( count($targets) == 0 ) $commands[] = 'CREATE' ;
		else {
			$q = getItemForWikipediaPage ( $params['wiki'] , $targets[0] ) ;
			if ( !isset($q) ) {
				$q = 'LAST' ;
				$commands[] = 'CREATE' ;
			} else {
				$wil = new WikidataItemList ;
				$wil->loadItem ( $q ) ;
				$i = $wil->getItem ( $q ) ;
				$id2q[$id] = $q ;
				print "ADDED $id TO EXISTING ITEM $q\n" ;
			}
		}
	} else {
		$commands[] = 'CREATE' ;
	}
	if ( $q == 'Q575759' ) { // HARDCODED generic memorial, do not touch!
		print "MEMORIAL\n" ;
		print_r ( $entry ) ;
		return ;
	}

	$label = trim ( fixWikidataStringParameter ( getFieldFromEntry ( $entry , 'name' ) . ' ' . getFieldFromEntry ( $entry , 'adresse' ) ) ) ;

	if ( !$i->hasLabelInLanguage($lang) ) {
		if ( $label != '' ) $commands[] = "$q\tL{$lang}\t\"{$label}\"" ;
	}

	if ( !$i->hasDescriptionInLanguage($lang) ) {
		$desc = getFieldFromEntry ( $entry , 'beschreibung' ) ;
		if ( $desc == '' ) $desc = getFieldFromEntry ( $entry , 'name' ) . '; ' . getFieldFromEntry ( $entry , 'datierung' ) ;
		$desc = fixWikidataStringParameter ( $desc ) ;
		$desc = enforceUniqueWikidataDescription ( $lang , $label , $desc ) ;
		if ( $desc != '' ) $commands[] = "$q\tD{$lang}\t\"{$desc}\"" ;
	}

	if ( !$i->hasClaims($params['id_prop']) ) { // ID
		$commands[] = "$q\t{$params['id_prop']}\t\"{$id}\"" ;
	}

	if ( !$i->hasClaims('P31') ) { // Instance of
		$p31 = getInstanceOf ( $entry ) ;
		if ( $p31 != '' ) $commands[] = "$q\tP31\t{$p31}" ;
	}

	if ( !$i->hasClaims('P18') ) { // Picture
		$image = getFieldFromEntry ( $entry , 'bild' ) ;
		if ( doesFileExistOnCommons($image) != '' ) $commands[] = "$q\tP18\t\"{$image}\"" ;
	}

	if ( !$i->hasClaims('P373') ) { // Commonscat
		$commonscat = getFieldFromEntry ( $entry , 'commonscat' ) ;
		if ( $commonscat != '' ) $commands[] = "$q\tP373\t\"{$commonscat}\"" ;
	}

	if ( !$i->hasClaims('P625') ) { // Coordinate
		$coordinate = getFieldFromEntry ( $entry , 'ns' ) . '/' . getFieldFromEntry ( $entry , 'ew' ) ;
		if ( $coordinate != '/' ) $commands[] = "$q\tP625\t@{$coordinate}" ;
	}

	if ( !$i->hasClaims('P571') ) { // Inception
		$year = '' ;
		if ( preg_match ( '/\b(\d{3,4})\b/' , getFieldFromEntry ( $entry , 'datierung-sort' ) , $m ) ) $year = $m[1] ;
		else if ( preg_match ( '/\b(\d{3,4})\b/' , getFieldFromEntry ( $entry , 'datierung' ) , $m ) ) $year = $m[1] ;
		if ( $year != '' ) $commands[] = "$q\tP571\t+{$year}-01-17T00:00:00Z/9" ;
	}

	foreach ( $bespoke_statements AS $statement ) {
		if ( !$i->hasClaims($statement['property']) ) {
			$value = fixWikidataStringParameter ( $statement['value'] ) ;
			$commands[] = "$q\t{$statement['property']}\t{$value}" ;
		}
	}

	if ( !$i->hasClaims('P969') ) { // street address
		$street = getFieldFromEntry ( $entry , 'adresse' ) ;
		$street = fixWikidataStringParameter ( $street ) ;
		if ( $street != '' ) {
			$cmd = "$q\tP969\t\"{$street}\"" ;
			$commands[] = $cmd ;
		}
	}

	if ( count($commands) == 0 ) return ; // Nothing to do

	$commands = implode ( "\n" , $commands ) ;
	$tmp_uncompressed = $qs->importData ( $commands , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;

	foreach ( $tmp['data']['commands'] AS $cmd ) {
		if ( isset($cmd) ) continue ;
		print "UNCOMPRESSED:\n" ;
		print_r ( $tmp_uncompressed ) ;
		print "\n\nCOMPRESSED:\n" ;
		print_r ( $tmp ) ;
		exit(0);
	}

	$qs->runCommandArray ( $tmp['data']['commands'] ) ;

	if ( $q == 'LAST' ) $q = 'Q' . preg_replace ( '/\D/' , '' , "{$qs->last_item}" ) ;
	$id2q[$id] = $q ;
}


function constructPetscanUrl ( $params ) {
	$petscan_parameters = [
		'language' => 'en' ,
		'project' => 'wikipedia' ,
		'depth' => '0' ,
		'categories' => '' ,
		'combination' => 'subset' ,
		'ns[0]' => '1' ,
		'show_redirects' => 'both' ,
		'edits[bots]' => 'both' ,
		'edits[anons]' => 'both' ,
		'edits[flagged]' => 'both' ,
		'subpage_filter' => 'either' ,
		'common_wiki' => 'auto' ,
		'wikidata_item' => 'no' ,
		'wpiu' => 'any' ,
		'cb_labels_yes_l' => '1' ,
		'cb_labels_any_l' => '1' ,
		'cb_labels_no_l' => '1' ,
		'format' => 'json' ,
		'output_compatability' => 'quick-intersection' ,
		'sortby' => 'none' ,
		'sortorder' => 'ascending' ,
		'min_redlink_count' => '1' ,
		'doit' => 'Do it!' ,
	] ;
	foreach ( $params['petscan'] AS $k => $v ) {
		$petscan_parameters[$k] = $v ;
	}
	$p2 = [] ;
	foreach ( $petscan_parameters AS $k => $v ) {
		if ( $v == '' ) continue ;
		$p2[] = urlencode($k) . '=' . urlencode($v) ;
	}
	$url = 'https://petscan.wmflabs.org/?' . implode ( '&' , $p2 ) ;
	return $url ;
}

function updateDatabaseFromPetScan ( $params ) {
	global $tfc , $main_table ;

	// Load pages in database
	$pages_in_db = [] ;
	$sql = "SELECT * FROM `$main_table` WHERE `group`='" . $tfc->dbt->real_escape_string($params['group']) . "'" ;
	$result = $tfc->getSQL ( $tfc->dbt , $sql ) ;
	while($o = $result->fetch_object()){
		$pages_in_db[$o->page_title] = $o ;
	}

	// Get current pages via PetScan
	$petscan_url = constructPetscanUrl ( $params ) ;
	$json = json_decode ( file_get_contents ( $petscan_url ) ) ;
	foreach ( $json->pages AS $page ) {
		if ( isset($pages_in_db[$page->page_title]) and 
			$pages_in_db[$page->page_title]->page_latest == $page->page_latest and  
			$pages_in_db[$page->page_title]->page_len == $page->page_len ) continue ; // Nothing has changed for that page
		$sql = '' ;
		if ( isset($pages_in_db[$page->page_title]) ) { // Page exists in DB but has changed
			$p = $pages_in_db[$page->page_title] ;
			$sql = "UPDATE `$main_table` SET " ;
			$sql .= " page_id={$page->page_id}," ;
			$sql .= " page_namespace={$page->page_namespace}," ;
			$sql .= " page_latest='{$page->page_latest}'," ;
			$sql .= " page_len={$page->page_len}," ;
			$sql .= " done=0" ;
			$sql .= " WHERE id={$p->id}" ;
		} else { // New page
			$sql = "INSERT IGNORE INTO `$main_table` (page_id,page_namespace,page_title,page_len,page_latest,`group`,done) VALUES (" ;
			$sql .= $page->page_id . ',' ;
			$sql .= $page->page_namespace . ',' ;
			$sql .= '"' . $tfc->dbt->real_escape_string($page->page_title) . '",' ;
			$sql .= $page->page_len . ',' ;
			$sql .= '"' . $page->page_latest . '",' ;
			$sql .= '"' . $tfc->dbt->real_escape_string($params['group']) . '",' ;
			$sql .= "0)" ;
		}
		$tfc->getSQL ( $tfc->dbt , $sql ) ;
	}
}

function getExistingItems ( $params ) { # Get existing items on Wikidata via SPARQL
	global $tfc , $id2q ;
	$sparql = "SELECT ?q ?val { ?q wdt:{$params['id_prop']} ?val }" ;
	$json_existing = $tfc->getSPARQL ( $sparql ) ;
	$id2q = [] ; // ID => Wikidata item
	foreach ( $json_existing->results->bindings AS $b ) {
		$id = $b->val->value ;
		if ( isset($params['formatID']) ) $id = $params['formatID'] ( $id ) ;
		$q = $tfc->parseItemFromURL ( $b->q->value ) ;
		$id2q[$id] = $q ;
	}
	return $id2q ;
}

function updateWikidataFromDatabase ( $params ) {
	global $tfc , $main_table ;

	// Load pages in database
	$pages_in_db = [] ;
	$sql = "SELECT * FROM `$main_table` WHERE `group`='" . $tfc->dbt->real_escape_string($params['group']) . "' AND done=0" ;
	$result = $tfc->getSQL ( $tfc->dbt , $sql ) ;
	while($o = $result->fetch_object()){
		$pages_in_db[$o->page_title] = $o ;
	}

	foreach ( $pages_in_db AS $page ) {
		print "Processing '{$page->page_title}'\n" ; $tfc->flush() ;
//if ( $page->page_title != 'Liste_der_Kulturdenkmale_in_Hoyerswerda' ) continue ; // TESTING
		$wikitext = $tfc->getWikiPageText ( $params['wiki'] , $page->page_title ) ;
//		print "Text retrieved\n" ; $tfc->flush() ;

		// Set bespoke statements based on page
		$bespoke_statements = [] ;
		$bespoke_statements[] = [ 'property'=>'P1435' , 'value'=>'Q11691318' ] ; # KULTURDENKMAL
		$bespoke_statements[] = [ 'property'=>'P17' , 'value'=>'Q183' ] ; # GERMANY
		$place_item = getPlaceItem ( $page->page_title , $wikitext , $params['wiki'] ) ;
		if ( isset($place_item) ) $bespoke_statements[] = [ 'property'=>'P131' , 'value'=>$place_item ] ;
//		print "Place item retrieved\n" ; $tfc->flush() ;

		// Parse and run entries
		$list = parseMultilineTemplates ( $wikitext , $params['entry_template'] , $params ) ;
		foreach ( $list AS $entry ) {
			$id = getFieldFromEntry ( $entry , 'id' ) ;
//			print "Processing entry $id ..." ; $tfc->flush() ;
			addOrAmendEntry ( $entry , $bespoke_statements , $params ) ;
//			print " done.\n" ; $tfc->flush() ;
		}

		// Mark as done
		$tfc->dbt = $tfc->openDBtool ( 'wikidata_cache_p' ) ;
		$sql = "UPDATE `$main_table` SET done=1 WHERE id={$page->id}" ;
//		print "Updating $main_table [ {$sql} ] ... " ; $tfc->flush() ;
		$tfc->getSQL ( $tfc->dbt , $sql ) ;
//		print "done.\n" ; $tfc->flush() ;
	}
}


$tfc = new ToolforgeCommon ( 'denkmallisten_sachsen_magnus' ) ;
$qs = getQS() ;
$tfc->dbt = $tfc->openDBtool ( 'wikidata_cache_p' ) ;

$id2q = getExistingItems ( $params ) ;
updateDatabaseFromPetScan ( $params ) ;
updateWikidataFromDatabase ( $params ) ;

?>