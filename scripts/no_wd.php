#!/usr/bin/php
<?PHP

include_once ( '/data/project/wikidata-todo/public_html/php/common.php' ) ;

$datadir = '/data/project/wikidata-todo/scripts/no_wd_data' ;

$j = json_decode ( file_get_contents ( 'http://en.wikipedia.org/w/api.php?action=query&meta=siteinfo&siprop=interwikimap&format=json' ) ) ;
$langs = array() ;
foreach ( $j->query->interwikimap AS $i ) {
	if ( !preg_match ( '/\/\/([a-z]+)\.wikipedia\.org/' , $i->url , $m ) ) continue ;
	$langs[$m[1]] = $m[1] ;
}

exec ( "rm -rf $datadir/*" ) ; // Cleanup

$fhs = fopen ( "$datadir/stats.tab" , 'w' ) ;
fwrite ( $fhs , "#Wiki\tNo_WD\tTotal\n" ) ;

foreach ( $langs AS $l ) {
	$db = @openDB ( $l , 'wikipedia' , true ) ;
	if ( !$db ) {
#		print "Can't open $l.wikipedia!\n" ;
		continue ;
	}
	
	$sql2 = '' ;
	if ( $l == 'de' ) $sql2 = "AND NOT EXISTS (SELECT * FROM categorylinks WHERE cl_from=page_id AND cl_to='Wikipedia:Falschschreibung')" ;

	$sql = "select page_title from page where page_namespace=0 and page_is_redirect=0 and page_title not in ( select replace(ips_site_page,' ','_') from wikidatawiki_p.wb_items_per_site where ips_site_id='".$l."wiki' ) $sql2" ;
	
	$cnt = 0 ;
	if(!$result = $db->query($sql)) {
		print "No wikidata table available for $l!\n" ;
		continue ;
//		die('There was an error running the query [' . $db->error . ']');
	}
	$fh = fopen ( "$datadir/wiki.$l.tab" , 'w' ) ;
	while($o = $result->fetch_object()){
		fwrite ( $fh , $o->page_title."\n" ) ;
		$cnt++ ;
	}
	fclose ( $fh ) ;
	
	$total = 1 ;
	$sql = "SELECT count(*) AS cnt FROM page WHERE page_namespace=0 $sql2" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$total = $o->cnt ;
	}
	
	fwrite ( $fhs , $l . "wiki\t$cnt\t$total\n" ) ;
	
	$db->close() ;
}

fclose ( $fhs ) ;

?>