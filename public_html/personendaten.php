<?PHP

require_once ( 'php/common.php' ) ;

$formkeys = array ( 'name' , 'desc' , 'staat' , 'sex' ,
'geb_art' , 'geb_tag1' , 'geb_monat1' , 'geb_jahr1' , 'geb_tag2' , 'geb_monat2' , 'geb_jahr2' , 'geb_ort' ,
'st_art' , 'st_tag1' , 'st_monat1' , 'st_jahr1' , 'st_tag2' , 'st_monat2' , 'st_jahr2' , 'st_ort'
) ;


$lang = get_request ( 'lang' , 'de' ) ;
foreach ( $formkeys AS $k ) $$k = trim ( get_request ( $k , '' ) ) ;

// Countries
$sparql = "SELECT ?country ?short ?countryLabel { ?country wdt:P31 wd:Q6256 . ?country wdt:P298 ?short . SERVICE wikibase:label { bd:serviceParam wikibase:language '$lang' . }  } ORDER BY ?countryLabel" ;
$j = getSPARQL ( $sparql ) ;
$countries = array() ;
$country2q = array() ;
foreach ( $j->results->bindings AS $d ) {
	$q = preg_replace ( '/^.+\/Q/' , 'Q' , $d->country->value ) ;
	$short = $d->short->value ;
	$name = $d->countryLabel->value ;
	$country2q[$short] = $q ;
	$countries[] = array ( $name , $q ) ;
}
//print "<pre>" ; print_r ( $countries ) ; print "</pre>" ; 
if ( $staat != '' and !preg_match ( '/^Q\d+$/' , $staat ) ) $staat = $country2q[strtoupper(staat)] ;


$days = '' ;
for ( $d = 1 ; $d <= 31 ; $d++ ) $days .= "<option>$d</option>" ;
$months = '<option value="0"></option>
<option value="1">Januar</option>
<option value="2">Februar</option>
<option value="3">März</option>
<option value="4">April</option>
<option value="5">Mai</option>
<option value="6">Juni</option>
<option value="7">Juli</option>
<option value="8">August</option>
<option value="9">September</option>
<option value="10">Oktober</option>
<option value="11">November</option>
<option value="12">Dezember</option>' ;

//$k0 = '<div class="container">' ;
//$img = "<div style='float:left;margin:10px;z-index:2'><img src='/persondata/gfx/placeholder.jpg' /></div>" ;
$h = get_common_header ( '' , "Personendaten" ) ;
//$h = str_replace ( $k0 , $img.$k0 , $h ) ;
$h .= "<h1>Wikipedia-Personensuche</h1>" ;

$h .= '<form method="post" class="form form-inline">
<table cellpadding="0" cellspacing="0" style="color:#626262;">
  <tbody><tr>
    <td><b>Name:</b></td>
    <td><input type="text" name="name" style="width:300px;" value=""></td>
  </tr>
  <tr>
    <td><b>Beschreibung:</b></td>
    <td><input type="text" name="desc" style="width:300px;" value=""></td>
  </tr>
  <tr>
    <td><b>Geboren:&nbsp;&nbsp;</b></td>
    <td>  
      <select class="custom-select"  size="1" name="geb_art" onchange="change_geb_art();">
        <option value="1">am</option>
        <option value="2">vor</option>
        <option value="3">nach</option>
        <option value="4">zwischen</option>
        <option value="5">unbekannt</option>      </select>
      <span id="erstes_datum1">
      <select class="custom-select"  size="1" name="geb_tag1" style="text-align:center;"><option value="0"></option>' . $days . '</select>
      <select class="custom-select"  size="1" name="geb_monat1" style="text-align:center;">'.$months.'</select>
      <input type="text" name="geb_jahr1" style="width:40px; text-align:center;" size="20" value="">
      </span>
      <span id="zweites_datum1" style="display:none;">
      und
      <select class="custom-select"  size="1" name="geb_tag2" style="text-align:center;"><option value="0"></option>' . $days . '</select>
      <select class="custom-select"  size="1" name="geb_monat2" style="text-align:center;">'.$months.'</select>
      <input type="text" name="geb_jahr2" style="width:40px; text-align:center;" size="20" value="">
      </span>
    </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;in:</td>
    <td><input type="text" name="geb_ort" style="width:300px;" value=""></td>
  </tr>
  <tr>
    <td><b>Gestorben:</b>&nbsp;&nbsp;</td>
    <td>
      <select class="custom-select"  size="1" name="st_art" onchange="change_st_art();">
        <option value="1">am</option>
        <option value="2">vor</option>
        <option value="3">nach</option>
        <option value="4">zwischen</option>
        <option value="5">lebt noch</option>      </select>
      <span id="erstes_datum2">
      <select class="custom-select"  size="1" name="st_tag1" style="text-align:center;">
        <option value="0"></option>' . $days . '</select>
      <select class="custom-select"  size="1" name="st_monat1" style="text-align:center;">'.$months.'</select>
      <input type="text" name="st_jahr1" style="width:40px; text-align:center;" size="20" value="">
      </span>
      <span id="zweites_datum2" style="display:none;">
      und
      <select class="custom-select"  size="1" name="st_tag2" style="text-align:center;"><option value="0"></option>' . $days . '</select>
      <select class="custom-select"  size="1" name="st_monat2" style="text-align:center;">'.$months.'</select>
      <input type="text" name="st_jahr2" style="width:40px; text-align:center;" size="20" value="">
      </span>
    </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;in:</td>
    <td><input type="text" name="st_ort" style="width:300px;" value=""></td>
  </tr>
  <tr>
    <td>
<!--      <div style="float:right; padding:2px;"><img src="gfx/flags/clearpixel.png" id="flag" alt="" style="width:20px; height:12px;"></div>-->
      <b>Staat:</b>&nbsp;&nbsp;
    </td>
    <td>
      <select class="custom-select"  name="staat" size="1" style="width:300px; float:left;" onchange="changeFlag(this);">
        <option value="clearpixel" selected="selected">Keine Beschränkung</option>' ;

	foreach ( $countries AS $c ) {
		$h .= "<option value='{$c[1]}'>{$c[0]}</option>" ;
	}

	$h .= '</select>
      
      
<script type="text/javascript">
<!--
  function changeFlag(country)
  {
    code = country.options[country.selectedIndex].value.toLowerCase();
    document.getElementById("flag").src = "gfx/flags/" + code + ".png";
    document.getElementById("flag").alt = "Flagge von " + country.options[country.selectedIndex].text;
  }

-->
</script>
      
    </td>
  </tr>
  <tr>
    <td><b>Geschlecht:</b></td>
    <td>
      <select class="custom-select"  name="sex" size="1" style="width:300px; float:left;">
        <option value="" selected="selected">Keine Beschränkung</option>      
        <option value="1">Männer</option>      
        <option value="2">Frauen</option>      </select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      &nbsp;<br>
      <input type="submit" value="Suchen" style="width:300px; background-color:#B2AD93; color:#FFFFFF; 
      border:2px solid white; cursor:pointer; font-weight:bold;">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td style="font-size:10pt; padding-top:15px;">
    In den freien Suchfeldern (Name, Beschreibung, Geburts- und Sterbeort) gilt folgende Syntax: Präzise
<!--    Jedes Wort muss entweder vollständig vorkommen oder es kann per <i>wort*</i> nach einem Wortanfang gesucht werden. -->
    </td>
  </tr>  
  </tbody></table>
</form>' ;

print $h ;
print get_common_footer() ;

?>